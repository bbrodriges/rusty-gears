//! A collection of convenient String functions based on &str methods.

pub fn find(haystack: &String, needle: &str) -> Option<uint> {
    haystack.as_slice().find_str(needle)
}

pub fn rfind(haystack: &String, needle: &str) -> Option<uint> {
    let mut result_index = None;

    let rhay: String  = haystack.as_slice().chars().rev().collect();
    let rneed: String = needle.chars().rev().collect();

    let rev_haystack = rhay.as_slice();
    let rev_needle   = rneed.as_slice();
    
    let rindex = rev_haystack.find_str(rev_needle);

    if rindex != None {
        let rev_index = rindex.unwrap() + needle.len();
        result_index = Some(haystack.len() - rev_index);
    }

    result_index
}

pub fn match_indices(haystack: &String, needle: &str) -> Vec<(uint, uint)> {
    haystack.as_slice().match_indices(needle).collect()
}

pub fn split(input: String, delim: &str) -> Vec<String> {
    let mut result_vec: Vec<String> = Vec::new();

    let mut parts = input.as_slice().split_str(delim);
    for part in parts {
        result_vec.push(part.into_string());
    }

    result_vec
}

pub fn splitn(input: String, delim: &str, count: uint) -> Vec<String> {
    let mut result_vec: Vec<String> = Vec::new();

    if count == 0 {
        return vec![input];
    }

    let parts = self::split(input, delim);
    if count >= parts.len() {
        return parts;
    }

    result_vec.push_all(parts.slice_to(count));
    result_vec.push(parts.slice_from(count).connect(delim));

    result_vec
}

pub fn starts_with(input: &String, starts: &str) -> bool {
    input.as_slice().starts_with(starts)
}

pub fn ends_with(input: &String, ends: &str) -> bool {
    input.as_slice().ends_with(ends)
}

pub fn has_substring(input: &String, substr: &str) -> bool {
    self::find(input, substr) != None
}