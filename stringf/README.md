sprintf
===========

A collection of convenient String functions based on &str methods.

Usage example
===========

```rust
pub mod sprintf;

fn main() {
	let test_string = String::from_str("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");

	println!(stringf::stringf::find(&test_string, "sit"));
	println!(stringf::stringf::rfind(&test_string, "et"));
	println!(stringf::stringf::starts_with(&test_string, "Lor"));
	println!(stringf::stringf::ends_with(&test_string, "it."));
```

Output:

```rust
Some(18)
Some(35)
true
true
```

Methods
===========
> **``fn find(haystack: &String, needle: &str) -> Option<uint>``**

> Returns index of first char of first occurance of needle in String.

> **``rfind(haystack: &String, needle: &str) -> Option<uint>``**

> Returns index of first char of last occurance of needle in String.

> **``match_indices(haystack: &String, needle: &str) -> Vec<(uint, uint)>``**

> Returns a vector of start/end indices pairs of any occurance needle in String.

> **``fn split(input: String, delim: &str) -> Vec<String>``**

> Splits String by given delimiter.

> **``fn splitn(input: String, delim: &str, count: uint) -> Vec<String>``**

> Splits String by given delimiter by _count_ occurances of delimiter.

> **``fn starts_with(input: &String, starts: &str) -> bool``**

> Checks if String starts with given substring.

> **``fn ends_with(input: &String, ends: &str) -> bool``**

> Checks if String ends with given substring.

> **``fn has_substring(input: &String, substr: &str) -> bool``**

> Checks if String has substring of given string.
