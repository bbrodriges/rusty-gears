//! A set of unique elements ordered by priority.

use std::fmt;

// Priority levels
pub static PRIORITY_HIGH: uint = 1;
pub static PRIORITY_LOW: uint = 0;

#[deriving(Clone)]
pub struct PrioritySet<T> {
	set: Vec<T>,
	// a metaphysical delimiter between last high priority and first low priority elements
	priority_border: uint,
    // a metaphysical delimiter between last untouched and first touched elements
    waste_border: uint
}

impl<T: PartialEq + Clone> PrioritySet<T> {
    pub fn new() -> PrioritySet<T> {
        PrioritySet {
        	set: Vec::new(),
        	priority_border: 0,
            waste_border: 0
        }
    }

    pub fn add(&mut self, item: T, priority: uint) {
    	if !self.set.contains(&item) {

            match priority {
                PRIORITY_HIGH => self.set.insert(0, item),
                _  => self.set.insert(self.priority_border, item)
            }

            // on high priority insert - moving priority border one element to the right
	    	if priority == PRIORITY_HIGH {
                self.priority_border = self.priority_border + 1;
            }

            // on any insert - moving waste border one element to the right
            self.waste_border = self.waste_border + 1;
	    }
    }

    pub fn get(&mut self) -> Option<T> {
        // if we have any untouched elements - proceeding
        if self.waste_border != 0 {
            // moving waste border one element left
            self.waste_border = self.waste_border - 1;

            // if we in high priority scope - moving priority border one element left
            if self.priority_border > 0 {
        	   self.priority_border = self.priority_border - 1;
            }

        	let elem = self.set.remove(0);
            let wasted_elem = elem.clone();
            // marking element as touched
            self.set.push(wasted_elem.unwrap());

            return elem;
        }

        None
    }

    pub fn true_len(&self) -> uint {
        self.set.len()
    }

    pub fn true_empty(&self) -> bool {
        self.set.is_empty()
    }
}

impl<T: fmt::Show> fmt::Show for PrioritySet<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    	self.set.slice_to(self.waste_border).fmt(f)
    }
}

impl<T> Collection for PrioritySet<T> {
    fn len(&self) -> uint {
        self.set.slice_to(self.waste_border).len()
    }

    fn is_empty(&self) -> bool {
        self.set.slice_to(self.waste_border).is_empty()
    }
}