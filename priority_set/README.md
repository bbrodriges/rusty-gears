PrioritySet
===========

A set of unique elements ordered by user defined priority. Priority set based on ``std::vec::Vec`` "splitted" into three parts using inner pointers.

Visual representaion:
```
+-----------------------------------------------------------------------------+
|  High priority part  |  Low priority part  |  Hidden touched elements part  |
+-----------------------------------------------------------------------------+
                       ^                     ^
priority delimiter -----                     ----- hidden part delimiter
```

Usage example
===========

```rust
use priority_set::PrioritySet;

pub mod priority_set;

fn main() {
	let mut set: PrioritySet<&str> = PrioritySet::new();
	set.add("high", priority_set::PRIORITY_HIGH);
	set.add("low", priority_set::PRIORITY_LOW);
    set.add("duplicate", priority_set::PRIORITY_LOW);
    set.add("duplicate", priority_set::PRIORITY_LOW);

	println!("{}", set);
	println!("{}", set.get());
	println!("{}", set);
}
```

Output:

```rust
[high, duplicate, low]
Some(high)
[duplicate, low]
```

Methods
===========
> **``fn new() -> PrioritySet<T>``**

> Constructs a new, empty PrioritySet.

> **``fn add(&mut self, item: T, priority: uint)``**

> Inserts item into PrioritySet with given priority. Silently ignores duplicate item.

> **``fn get(&mut self) -> Option<T>``**

> Removes first item from PrioritySet and returns it. Additionaly inserts copy of this item into hidden part.

> **``fn len(&self) -> uint``**

> Returns number of items in PrioritySet without hiddent part.

> **``fn is_empty(&self) -> bool``**

> Returns true if PrioritySet contains at least one item excluding hidden part.

> **``fn true_len(&self) -> uint``**

> Returns number of items in PrioritySet including hiddent part.

> **``fn true_empty(&self) -> bool``**

> Returns true if PrioritySet contains at least one item including hidden part.
