use priority_set::priority_set::{PrioritySet, PRIORITY_HIGH, PRIORITY_LOW};

mod priority_set;
mod stringf;

fn main() {
	// Testing PrioritySet module
	let mut set: PrioritySet<&str> = PrioritySet::new();

	set.add("high", PRIORITY_HIGH);
	set.add("low", PRIORITY_LOW);
    set.add("duplicate", PRIORITY_LOW);
    set.add("duplicate", PRIORITY_LOW);

	assert_eq!(set.len(), 3);
	assert_eq!(set.get(), Some("high"));
	assert_eq!(set.len(), 2);
	assert_eq!(set.true_len(), 3);
	assert_eq!(set.get(), Some("duplicate"));
	assert_eq!(set.get(), Some("low"));
	assert_eq!(set.is_empty(), true);
	assert_eq!(set.true_empty(), false);

	// Testing stringf module
	let test_string = String::from_str("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");

	assert_eq!(stringf::stringf::find(&test_string, "sit"), Some(18));
	assert_eq!(stringf::stringf::find(&test_string, "Awesome"), None);

	assert_eq!(stringf::stringf::rfind(&test_string, "et"), Some(35));
	assert_eq!(stringf::stringf::rfind(&test_string, "Super cow"), None);

	assert_eq!(stringf::stringf::match_indices(&test_string, "et"), vec![(24, 26), (35, 37)]);
	assert_eq!(stringf::stringf::starts_with(&test_string, "Lor"), true);
	assert_eq!(stringf::stringf::ends_with(&test_string, "it."), true);

	assert_eq!(stringf::stringf::has_substring(&test_string, "sit"), true);
	assert_eq!(stringf::stringf::has_substring(&test_string, "Wow"), false);

	let mut res = vec!["Lorem ipsum dolor sit ".to_string(), ", consectetur adipiscing elit.".to_string()];
	assert_eq!(stringf::stringf::split(test_string.clone(), "amet"), res);

	res = vec!["Lorem".to_string(), "ipsum".to_string(),  "dolor".to_string(), "sit amet, consectetur adipiscing elit.".to_string()];
	assert_eq!(stringf::stringf::splitn(test_string.clone(), " ", 3), res);
}