[![Build Status](https://travis-ci.org/bbrodriges/rusty-gears.svg?branch=master)](https://travis-ci.org/bbrodriges/rusty-gears)

rusty-gears
===========

A collection of simple but useful Rust modules.

Dependencies
===========
* [Rust Nightly](http://www.rust-lang.org/install.html)
